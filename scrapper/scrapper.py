import logging
import time
from typing import Any

import requests
from selenium import webdriver  # Webdriver de Selenium qui permet de contrôler un navigateur
from selenium.webdriver import ActionChains
from selenium.webdriver.chrome.service import Service as ChromeService
from webdriver_manager.chrome import ChromeDriverManager

from GuideEurodialPage import GuideEurodialPage
from commun.database.db_queries import add_pays, add_hopital, add_adresse, add_location, add_region, \
    get_id_pays_by_name, \
    get_id_region_by_name
from dao.centre import Centre

API_URL = "https://api-adresse.data.gouv.fr/search/?q="

logger = logging.getLogger(__name__)
# Chrome Navigateur
options = webdriver.ChromeOptions()
# options.add_argument('--headless=new')
# options.add_experimental_option('useAutomationExtension', False)
options.add_experimental_option("detach", False)
options.add_experimental_option('excludeSwitches', ['disable-popup-blocking'])
options.add_argument("window-size=1200x600")

options.add_experimental_option("detach", False)

driver = webdriver.Chrome(service=ChromeService(ChromeDriverManager().install()),
                          options=options)
# Edge Navigateur
# options = webdriver.EdgeOptions()
# driver = webdriver.Edge(options=options)

page = GuideEurodialPage(driver)

# True scrppe toutes les données de la France
# False, que les régions Alsace, Midi-Pyrénée et Ile de France
SCRAP_ALL_FRANCE = True
is_page_suivante = True
num_page = 1

# Load the page
page.load()


def close_information_popup() -> None:
    popup = page.get_popup_confirmation()
    if popup is not None:
        ActionChains(driver).move_to_element(popup).perform()
        accept_button = page.get_button_close_popup_confirmation()
        ActionChains(driver).move_to_element(accept_button).click().perform()

        # accept_button.click()


def extract_hopitaux() -> None:
    content = page.get_content_hopitaux()
    if content is not None:
        hopitaux = page.get_bloc_centre(content)
        for hopital in hopitaux:
            centre = ajoute_hopital(hopital)
            insert_hopital(centre)


def nettoyage_texte(adresse: str):
    return adresse.replace('­', "")


def replace_space(adresse):
    return adresse.replace(' ', '+')


def adresse2gps(adresse, code_postal) -> tuple[None, None] | tuple[Any, Any]:
    response = requests.get(f'{API_URL}{replace_space(adresse)}&postcode={code_postal}')

    print(replace_space(adresse), code_postal)
    if response.status_code == 200:
        data = response.json()
        if (data is not None and data['features'] is not None and len(data['features']) > 0 and
                data['features'][0]['geometry'] is not None and data['features'][0]['geometry'][
                    'coordinates'] is not None):
            return data['features'][0]['geometry']['coordinates'][0], data['features'][0]['geometry']['coordinates'][1]

    return None, None


def ajoute_hopital(hopital) -> Centre:
    """
    :param hopital: The hopital object containing information about a hospital.
    :return: The centre object representing the hospital.
    This method takes in a hopital object, extracts the necessary information such as name, address, city,
    country, region, and phone number from the hopital object. It then cleans the extracted text
    and splits it into separate lines. The required information is extracted from each line using split() and
    strip() methods.
    If the country is "France", it extracts the postal code from the city line and removes it from the city line.
    The extracted information is then printed.
    The address and postal code are used to get the latitude and longitude coordinates using the adresse2gps() method.
    A centre object is created with all the extracted information and is printed. This object represents the hospital.
    The centre object is then returned.
    Example usage:
    hopital = {
        "text": "Hopital Name\nAddress // Line\nCity // Line\nCountry // Line\nRegion // Line\nTelephone // Line"
    }
    ajoute_hopital(hopital)
    """
    content_hopitaux = nettoyage_texte(hopital.text).split("\n")
    nom_hopital = content_hopitaux[0].strip()
    adresse_ligne = content_hopitaux[1].split("//")[1].strip()
    ville_ligne = content_hopitaux[2].split("//")[1].strip()
    pays_ligne = content_hopitaux[3].split("//")[1].split("-")[0].strip()
    region_ligne = content_hopitaux[4].split("//")[1].strip()
    telephone_ligne = content_hopitaux[5].split("//")[1].strip()
    cdp = ""
    if pays_ligne == 'France':
        cdp = ville_ligne[0:5].strip()
        ville_ligne = ville_ligne[6:].strip()
    print(f'{nom_hopital}, {adresse_ligne}, {cdp}, {ville_ligne}, {region_ligne}, {pays_ligne}, {telephone_ligne}')
    logger.info(
        f'{nom_hopital}, {adresse_ligne}, {cdp}, {ville_ligne}, {region_ligne}, {pays_ligne}, {telephone_ligne}')
    longitude, latitude = adresse2gps(adresse_ligne, cdp)
    time.sleep(5)

    centre = Centre()
    centre.nom = nom_hopital
    centre.adresse = adresse_ligne
    centre.ville = ville_ligne
    centre.region = region_ligne
    centre.code_postal = cdp
    centre.pays = pays_ligne
    centre.telephone = telephone_ligne
    centre.lat = latitude
    centre.lon = longitude

    print(str(centre))
    logger.info(str(centre))
    return centre


def page_suivante():
    """
    Advances to the next page if the next button is displayed.
    Increments the page number by 1 and prints the new page number.
    Sets the flag "is_page_suivante" to True if the next page is available, else sets it to False.
    Pauses execution for 5 seconds.
    """
    global is_page_suivante, num_page

    button = page.get_bouton_suivant()
    if button.is_displayed():
        ActionChains(driver).move_to_element(button).click().perform()
        # button.click()
        num_page = num_page + 1
        print("Page", num_page)
        is_page_suivante = True
    else:
        is_page_suivante = False

    time.sleep(5)


def insert_hopital(hopital: Centre) -> None:
    """
    Inserts a hospital into the database.

    :param hopital: The hospital object to be inserted.
    :return: None
    """
    try:
        id_pays = get_id_pays_by_name(hopital.pays)
        if id_pays is None:
            id_pays = add_pays(hopital.pays)
        if id_pays is not None:
            id_region = get_id_region_by_name(hopital.region)
            if id_region is None:
                id_region = add_region(hopital.region, id_pays)

            id_adresse = add_adresse(hopital.adresse, hopital.code_postal, hopital.ville, id_region)
            if id_adresse is not None:
                add_hopital(hopital.nom, hopital.telephone, id_adresse)
                add_location(hopital.lon, hopital.lat, id_adresse)

    except Exception as e:
        print(e)
        logger.error(e)


def active_france_filter() -> None:
    """
    Activates the France filter by clicking on the corresponding link.

    :return: None
    """
    cherch = page.select_centre_element()
    cherch.click()
    lnk_fran = page.link_france_element()

    (
        ActionChains(driver)
        .move_to_element(lnk_fran)
        .click(lnk_fran)
        .perform()
    )


def active_idf_filter() -> None:
    """
    Filter the active IDF on the page.

    :return: None
    """
    search = page.select_region_element()
    search.click()
    lnk_idf = page.link_idf_element()

    (
        ActionChains(driver)
        .move_to_element(lnk_idf)
        .click(lnk_idf)
        .perform()
    )


def active_alsace_filter() -> None:
    """Checks the search region and applies the Alsace filter.

    :return: None
    """
    search = page.select_region_element()
    search.click()
    driver.implicitly_wait(5)
    lnk_idf = page.link_alsace_element()

    (
        ActionChains(driver)
        .move_to_element(lnk_idf)
        .click(lnk_idf)
        .perform()
    )


def active_midi_pyrenees_filter() -> None:
    """
    Clicks on the region element, waits for 5 seconds, and then clicks on the link for Midi-Pyrenees.
    """
    search = page.select_region_element()
    ActionChains(driver).click(search).perform()
    # search.click()
    driver.implicitly_wait(5)
    lnk_idf = page.link_midi_pyrenees_element()

    (
        ActionChains(driver)
        .move_to_element(lnk_idf)
        .click(lnk_idf)
        .perform()
    )


if __name__ == '__main__':
    logging.basicConfig(filename='../myapp.log', level=logging.INFO)
    logger.info('Started')

    try:
        close_information_popup()

        active_france_filter()
        time.sleep(2)

        if not SCRAP_ALL_FRANCE:
            active_idf_filter()
            time.sleep(2)

        while is_page_suivante:
            extract_hopitaux()
            driver.implicitly_wait(5)

            page_suivante()

        if not SCRAP_ALL_FRANCE:
            print("Fin extraction - Ile de France")

            active_alsace_filter()
            time.sleep(2)
            is_page_suivante = True

            while is_page_suivante:
                extract_hopitaux()
                driver.implicitly_wait(5)

                page_suivante()

            print("Fin extraction - Alsace")
            is_page_suivante = True
            active_midi_pyrenees_filter()
            time.sleep(2)

            while is_page_suivante:
                extract_hopitaux()
                driver.implicitly_wait(5)

                page_suivante()

            print("Fin extraction - Midi Pyrenees")

    except Exception as e:
        print(e)
        driver.quit()

    driver.quit()
    logger.info('Finished')
