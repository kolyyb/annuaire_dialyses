from dataclasses import dataclass
from typing import Optional


@dataclass
class Centre:
    nom: Optional[str] = None
    adresse: Optional[str] = None
    code_postal: Optional[str] = None
    ville: Optional[str] = None
    region: Optional[str] = None
    pays: Optional[str] = None
    telephone: Optional[str] = None
    lat: Optional[float] | None = None
    lon: Optional[float] | None = None

    def __str__(self) -> str:
        return (f'Centre(nom={self.nom}, '
                f'adresse={self.adresse}, ' 
                f'ville={self.ville}, '
                f'region={self.region}, '
                f'pays={self.pays}, '
                f'latitude={self.lat}, '
                f'longitude={self.lon}, '
                f'code_postal={self.code_postal}, '
                f')'
                )
