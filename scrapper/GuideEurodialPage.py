from selenium.webdriver.common.by import By  # Permet d'accéder aux différents élements de la page web


class GuideEurodialPage(object):
    def __init__(self, driver):
        self.driver = driver
        self.url = "https://idotourisme.com/guide-eurodial/"

    # Définition des constantes definissant les elements du DOM liées aux éléments de la page HTML
    POPUP_CONFIRMATION = "#cookie-law-info-bar"
    BUTTON_CLOSE_POPUP_CONFIRMATION = "#cookie_action_close_header"
    CONTENT_HOPITAUX = ".dp-dfg-items"
    ARTICLE_CENTRE = "article.dp-dfg-item.project_category-france"
    PAGE_SUIVANTE = "li[class*='next-posts']"

    def load(self):
        """
        Load the specified URL using the WebDriver instance.

        :return: None
        """
        self.driver.get(self.url)

    def get_popup_confirmation(self):
        """
        Get the popup confirmation element.

        :return: The popup confirmation element.
        """
        return self.driver.find_element(By.CSS_SELECTOR, self.POPUP_CONFIRMATION)

    def get_button_close_popup_confirmation(self):
        """
        Returns the close popup confirmation button element.

        :return: The close popup confirmation button element.
        """
        return self.driver.find_element(By.CSS_SELECTOR, self.BUTTON_CLOSE_POPUP_CONFIRMATION)

    def get_content_hopitaux(self):
        """
        Method to get the content of hopitaux using CSS selector.

        :return: WebElement representing the content of hopitaux.
        """
        return self.driver.find_element(By.CSS_SELECTOR, self.CONTENT_HOPITAUX)

    def get_bloc_centre(self, parent):
        """
        :param parent: The parent element on the page where the elements will be searched from.
        :return: A list of elements that match the CSS selector for BLOC_CENTRE_FRANCE.
        """
        return parent.find_elements(By.CSS_SELECTOR, self.ARTICLE_CENTRE)

    def get_bouton_suivant(self):
        """
        Returns the last link element found using the given CSS selector for the "page suivante" button.

        :return: The last link element found for the "page suivante" button.
        """
        links = self.driver.find_elements(By.CSS_SELECTOR, self.PAGE_SUIVANTE)
        return links[-1]

    def select_centre_element(self):
        return self.driver.find_element(By.CSS_SELECTOR, "div[data-parent='0'] p[data-content='Chercher un centre']")

    def link_france_element(self):
        return self.driver.find_element(By.CSS_SELECTOR, "a[data-term-id='94']")

    def link_pagination(self):
        return self.driver.find_element(By.CSS_SELECTOR, "li[class*='next-posts'] a[data-page='5']")

    def select_region_element(self):
        return self.driver.find_element(By.CSS_SELECTOR, "div[data-parent='94'] p[data-content='Chercher un centre']")

    def link_idf_element(self):
        return self.driver.find_element(By.CSS_SELECTOR, "a[data-term-id='237']")

    def link_midi_pyrenees_element(self):
        return self.driver.find_element(By.CSS_SELECTOR, "a[data-term-id='244']")

    def link_alsace_element(self):
        return self.driver.find_element(By.CSS_SELECTOR, "a[data-term-id='232']")

    def li_pagination_direction(self):
        return self.driver.find_element(By.CSS_SELECTOR, "li[class*='next-posts']")
