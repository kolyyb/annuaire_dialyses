from commun.database.db_queries import get_all_hopital_by_dept
from api.models.Hopital_with_address import Hopital_with_address


async def all_hospital_by_dept(cdp: str) -> list[Hopital_with_address]:
    hospital_result = get_all_hopital_by_dept(cdp)

    if not hospital_result or hospital_result == []:
        raise Exception("No hospital found")

    hospitals = []
    for hospital in hospital_result:
        hopital_with_address = Hopital_with_address()
        hopital_with_address.nom_hopital = hospital['nom_hopital']
        hopital_with_address.adresse = hospital['adresse']
        hopital_with_address.cdp = hospital['cdp']
        hopital_with_address.ville = hospital['ville']
        hopital_with_address.tel = hospital['tel']
        hopital_with_address.region = hospital['nom_region']
        hopital_with_address.pays = hospital['nom_pays']
        hopital_with_address.latitude = hospital['latitude']
        hopital_with_address.longitude = hospital['longitude']
        hospitals.append(hopital_with_address)
    return hospitals
