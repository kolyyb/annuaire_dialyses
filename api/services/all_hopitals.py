from api.models.Hopital_with_address_and_dispo import Hopital_with_address_and_dispo
from commun.database.db_queries import get_all_hopital_by_dispo


async def all_hopitals():
    hospitals = get_all_hopital_by_dispo(None, None, None, None)
    if not hospitals or len(hospitals) == 0:
        raise Exception("No hospitals found")
    hospitals_list = []
    for hospital in hospitals:
        hopital_with_address_and_dispo = Hopital_with_address_and_dispo()
        hopital_with_address_and_dispo.nom_hopital = hospital['nom_hopital']
        hopital_with_address_and_dispo.adresse = hospital['adresse']
        hopital_with_address_and_dispo.cdp = hospital['cdp']
        hopital_with_address_and_dispo.ville = hospital['ville']
        hopital_with_address_and_dispo.tel = hospital['tel']
        hopital_with_address_and_dispo.region = hospital['nom_region']
        hopital_with_address_and_dispo.pays = hospital['nom_pays']
        hopital_with_address_and_dispo.latitude = hospital['latitude']
        hopital_with_address_and_dispo.longitude = hospital['longitude']
        hopital_with_address_and_dispo.is_dispo = 'disponible' if hospital['is_dispo'] == 1 else 'indisopnible'
        hopital_with_address_and_dispo.date_debut = hospital['date_debut']
        hopital_with_address_and_dispo.date_fin = hospital['date_fin']
        hopital_with_address_and_dispo.num_semaine = hospital['id_num_semaine']
        hospitals_list.append(hopital_with_address_and_dispo)

    return hospitals_list
