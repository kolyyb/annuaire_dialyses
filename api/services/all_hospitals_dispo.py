from datetime import datetime
from typing import List

from api.models.Disponibilite_model import Disponibilite_model
from api.models.Hopital_with_address_and_dispo import Hopital_with_address_and_dispo
from commun.database.db_queries import get_all_hopital_by_dispo, get_week_number, get_disponibilite_by_id



async def all_hospital_dispo(is_libre: str | None = None, region: str | None = None, departement: str | None = None,
                             num_semaine: str | None = None, date_debut: str | None = None,
                             date_fin: str | None = None) -> List[
    Hopital_with_address_and_dispo]:
    if is_libre == 'disponible' or is_libre == 'indisponible':
        is_libre = 1 if is_libre == 'disponible' else 0
    if is_libre == 'true' or is_libre == 'false':
        is_libre = 1 if is_libre == 'true' else 0

    weeks_nb = []
    if not num_semaine:
        date_debut = datetime.strptime(date_debut, "%Y-%m-%d") if date_debut else None
        date_fin = datetime.strptime(date_fin, "%Y-%m-%d") if date_fin else None
        weeks_nb = get_week_number(date_debut=date_debut, date_fin=date_fin)
        if len(weeks_nb) > 0:
            weeks_nb = [weeks_nb[0]]
    else:
        weeks_nb.append({'id_num_semaine': int(num_semaine)})
    hospitals_dispo = []

    for week_nb in weeks_nb:
        hospitals = get_all_hopital_by_dispo(region=region, departement=departement, is_libre=is_libre,
                                             week_nb=week_nb['id_num_semaine'])

        print(f'status={is_libre}, region={region}, departement={departement}, week_nb={num_semaine}')

        for hospital in hospitals:
            hopital_with_address_and_dispo = Hopital_with_address_and_dispo()
            hopital_with_address_and_dispo.nom_hopital = hospital['nom_hopital']
            hopital_with_address_and_dispo.adresse = hospital['adresse']
            hopital_with_address_and_dispo.cdp = hospital['cdp']
            hopital_with_address_and_dispo.ville = hospital['ville']
            hopital_with_address_and_dispo.tel = hospital['tel']
            hopital_with_address_and_dispo.region = hospital['nom_region']
            hopital_with_address_and_dispo.pays = hospital['nom_pays']
            hopital_with_address_and_dispo.latitude = hospital['latitude']
            hopital_with_address_and_dispo.longitude = hospital['longitude']
            hopital_with_address_and_dispo.is_dispo = 'disponible' if hospital['is_dispo'] == 1 else 'indisponible'
            hopital_with_address_and_dispo.date_debut = hospital['date_debut']
            hopital_with_address_and_dispo.date_fin = hospital['date_fin']
            hopital_with_address_and_dispo.num_semaine = hospital['id_num_semaine']

            resultat_disponibilites = get_disponibilite_by_id(hospital['id'], date_debut, date_fin, 5)

            if resultat_disponibilites is not None:
                for dispo_item in resultat_disponibilites:
                    disponibilite_model = Disponibilite_model()
                    disponibilite_model.date_fin = dispo_item['date_fin'].strftime('%d/%m/%Y')
                    disponibilite_model.date_debut = dispo_item['date_debut'].strftime('%d/%m/%Y')
                    disponibilite_model.num_semaine = dispo_item['num_semaine']
                    disponibilite_model.is_disponibilite = 'disponible' if dispo_item[
                                                                               'is_dispo'] == 1 else 'indisponible'

                    hopital_with_address_and_dispo.disponibilites.append(disponibilite_model)
            hospitals_dispo.append(hopital_with_address_and_dispo)

    return hospitals_dispo
