from commun.database.db_queries import get_list_of_region
from api.models.Region import Region


async def all_regions():
    regions = get_list_of_region()
    if not regions or len(regions) == 0:
        raise Exception("No Regions found ")

    regions_list = []
    for region in regions:
        region_temp = Region()
        region_temp.nom_region = region['nom_region']
        region_temp.id = region['id']

        regions_list.append(region_temp)

    return regions_list
