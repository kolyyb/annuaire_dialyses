from commun.database.db_queries import get_hopital_by_id, get_disponibilite_by_id
from api.models.Disponibilite_model import Disponibilite_model
from api.models.Hopital_with_address import Hopital_with_address


async def hopital_detail(id_hopital: int):
    hospital = get_hopital_by_id(id_hopital)

    hopital = Hopital_with_address()
    hopital.nom_hopital = hospital['nom_hopital']
    hopital.adresse = hospital['adresse']
    hopital.ville = hospital['ville']
    hopital.tel = hospital['tel']
    hopital.pays = hospital['nom_pays']
    hopital.region = hospital['nom_region']
    hopital.cdp = hospital['cdp']
    hopital.latitude = hospital['latitude']
    hopital.longitude = hospital['longitude']

    resultat_disponibilites = get_disponibilite_by_id(id_hopital)

    if resultat_disponibilites is not None:
        for dispo_item in resultat_disponibilites:
            disponibilite_model = Disponibilite_model()
            disponibilite_model.date_fin = dispo_item['date_fin'].strftime('%d/%m/%Y')
            disponibilite_model.date_debut = dispo_item['date_debut'].strftime('%d/%m/%Y')
            disponibilite_model.num_semaine = dispo_item['num_semaine']
            disponibilite_model.is_disponibilite = 'disponible' if dispo_item['is_dispo'] == 1 else 'indisponible'

            hopital.disponibilites.append(disponibilite_model)

    return hopital
