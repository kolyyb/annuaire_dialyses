from commun.database.db_queries import get_all_date_week
from api.models import Week_date


async def all_week_number() -> list[Week_date]:
    weeks = get_all_date_week()
    if not weeks or len(weeks) == 0:
        raise Exception("No weeks found")

    weeks_list = []
    for week in weeks:
        week_date = Week_date()
        week_date.id = week['id']
        week_date.num_semaine = week['id_num_semaine']
        week_date.date_debut = week['date_debut'].strftime('%d/%m/%Y')
        week_date.date_fin = week['date_fin'].strftime('%d/%m/%Y')
        weeks_list.append(week_date)

    return weeks_list
