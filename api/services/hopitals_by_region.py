from commun.database.db_queries import get_all_hopital_by_region
from api.models import Hopital_with_address_and_dispo


async def HopitalsByRegion(region: str):
    hospitals = get_all_hopital_by_region(region)
    if not hospitals or len(hospitals) == 0:
        raise Exception("No Hopitals found for region {}".format(region))

    hospitals_list = []
    for hospital in hospitals:
        hopital_with_address_and_dispo = Hopital_with_address_and_dispo()
        hopital_with_address_and_dispo.nom_hopital = hospital['nom_hopital']
        hopital_with_address_and_dispo.adresse = hospital['adresse']
        hopital_with_address_and_dispo.cdp = hospital['cdp']
        hopital_with_address_and_dispo.ville = hospital['ville']
        hopital_with_address_and_dispo.tel = hospital['tel']
        hopital_with_address_and_dispo.region = hospital['nom_region']
        hopital_with_address_and_dispo.pays = hospital['nom_pays']
        hopital_with_address_and_dispo.latitude = hospital['latitude']
        hopital_with_address_and_dispo.longitude = hospital['longitude']

        hospitals_list.append(hopital_with_address_and_dispo)

    return hospitals_list
