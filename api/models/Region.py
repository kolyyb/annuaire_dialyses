from pydantic import BaseModel


class Region(BaseModel):
    id: int | None = None
    nom_region: str | None = None
