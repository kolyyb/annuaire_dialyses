from datetime import date

from api.models.Hopital_with_address import Hopital_with_address


class Hopital_with_address_and_dispo(Hopital_with_address):
    is_dispo: int | str | None = None
    date_debut: date | None = None
    date_fin: date | None = None
    num_semaine: int | None = None
