from pydantic import BaseModel


class Disponibilite_model(BaseModel):
    num_semaine: int | None = None
    date_debut: str | None = None
    date_fin: str | None = None
    is_disponibilite: str | None = None
