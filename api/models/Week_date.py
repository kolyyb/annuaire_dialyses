from pydantic import BaseModel


class Week_date(BaseModel):
    id: int | None = None
    num_semaine: int | None = None
    date_debut: str | None = None
    date_fin: str | None = None
