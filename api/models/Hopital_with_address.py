from typing import List

from pydantic import BaseModel

from api.models.Disponibilite_model import Disponibilite_model


class Hopital_with_address(BaseModel):
    nom_hopital: str | None = None
    adresse: str | None = None
    cdp: str | None = None
    ville: str | None = None
    tel: str | None = None
    region: str | None = None
    pays: str | None = None
    latitude: float | None = None
    longitude: float | None = None
    disponibilites: List[Disponibilite_model] | None = []
