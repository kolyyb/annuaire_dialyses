from typing import List, Any, Coroutine

import uvicorn
from fastapi import FastAPI, HTTPException, status

from api.models.Hopital_with_address import Hopital_with_address
from api.models.Hopital_with_address_and_dispo import Hopital_with_address_and_dispo
from api.models.Region import Region
from api.models.Week_date import Week_date
from api.services.all_hopitals import all_hopitals
from api.services.all_hospital_by_dept import all_hospital_by_dept
from api.services.all_hospitals_dispo import all_hospital_dispo
from api.services.all_regions import all_regions
from api.services.all_week_number import all_week_number
from api.services.hopital_detail import hopital_detail
from api.services.hopitals_by_region import HopitalsByRegion


app = FastAPI(debug=True)


@app.get("/region/", status_code=200, response_model=List[Region])
async def get_all_regions() -> List[Region]:
    try:
        return await all_regions()
    except Exception as e:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail="Not found")


@app.get("/hopital/all/", status_code=200, response_model=List[Hopital_with_address])
async def get_alls() -> Coroutine[Any, Any, Any]:
    try:
        hospitals = await all_hopitals()
        return hospitals
    except Exception as e:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail="Not found")


@app.get("/hopital/{region}", status_code=200, response_model=List[Hopital_with_address])
async def get_hopital_by_region(region: str) -> Coroutine[Any, Any, Any]:
    try:
        return await HopitalsByRegion(region=region)
    except Exception as e:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail="Not found")


@app.get("/hopital/departement/{cdp}", status_code=200, response_model=List[Hopital_with_address])
async def get_hopital_by_dept(cdp: str) -> list[Hopital_with_address]:
    try:
        return await all_hospital_by_dept(cdp)
    except Exception as e:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail="Not found")


@app.get("/hopital/dispo/", status_code=status.HTTP_200_OK, response_model=List[Hopital_with_address_and_dispo])
async def get_all_hopital_dispo(is_libre: str | None = None, region: str | None = None, departement: str | None = None,
                                num_semaine: str | None = None, date_debut: str | None = None,
                                date_fin: str | None = None) -> List[
    Hopital_with_address_and_dispo]:
    return await all_hospital_dispo(is_libre, region, departement, num_semaine, date_debut, date_fin)


@app.get("/week_number/", status_code=200, response_model=List[Week_date])
async def get_all_week_number() -> list[Week_date]:
    try:
        return await all_week_number()
    except Exception as e:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail="Not found")


@app.get("/hopital/detail/", status_code=200, response_model=Hopital_with_address)
async def get_hopital_detail(id: int | None) -> Hopital_with_address:
    return await hopital_detail(id)


if __name__ == "__main__":
    uvicorn.run(app, host="0.0.0.0", port=8000)
