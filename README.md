# Annuaire des centres de dialyses

_Carte de France affichant tous les centres de néphrologie de France acceptant les vacanciers et disponibilité_

Le projet est constitué de 3 modules:
- le scrapper (répertoire scrapper)
- l'API (répertoire api)
- du front en Python Dash (répertoire dash_front)

## Scrapper
le scrapper s'exécute via la commande
```sh
python3 scrapper.py
```
dans le répertoire scrapper.

**Attention : Temps estimé du scrapping : 1 heure pour toutes les regions de France (1000)**

## CRUD
l'API s'exécute via la commande
```sh
python3 crud.py
```

ou

```sh
fastapi dev crud.py
```
dans le répertoire api

## Front
le dash s'exécute via la commande
```sh
python3 dash_front.py
```
dans le répertoire dash_front.
Le programme crud doit être en cours d'exécution.

## Ports utilisés: 
- 8000 par l'API
- 8050, par dash

## Base de données 
- Db : MySql
- Schéma de la base annuaire
- fichier db_structure.sql

## Autres fichiers
- insert_weeks.py : ajout des numéros de semaine et des dates de début et de fin de période
- fake_calendar.py: ajout des dates de disponibilités ou non dans la db