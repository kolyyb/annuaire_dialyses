from datetime import datetime
from typing import List

import mysql.connector as mysql


def add_hopital(nom: str, tel: str, id_adresse: int) -> int:
    conn = mysql.connect(host='localhost', user='root', password='pass', database='annuaire')
    cur = conn.cursor(dictionary=True, prepared=True)
    query = """INSERT INTO annuaire.hopital(nom_hopital, tel, id_adresse) VALUES (%s, %s, %s);"""
    cur.execute(query, (nom, tel, id_adresse))
    conn.commit()
    conn.close()
    return cur.lastrowid


def add_date_dispo(date_debut: str, date_fin: str, id_num_semaine: int) -> int:
    conn = mysql.connect(host='localhost', user='root', password='pass', database='annuaire')
    cur = conn.cursor(dictionary=True, prepared=True)
    query = """INSERT INTO annuaire.date_dispo(date_debut, date_fin, id_num_semaine) VALUES (%s, %s, %s);"""
    cur.execute(query, (date_debut, date_fin, id_num_semaine))
    conn.commit()
    cur.close()
    return cur.lastrowid


def add_adresse(adresse: str, cdp: str, ville: str, id_region: int) -> int:
    conn = mysql.connect(host='localhost', user='root', password='pass', database='annuaire')
    cur = conn.cursor(dictionary=True, prepared=True)
    query = """INSERT INTO annuaire.adresse(adresse, cdp, ville, id_region) VALUES (%s, %s, %s, %s);"""
    cur.execute(query, (adresse, cdp, ville, id_region))
    conn.commit()
    conn.close()
    return cur.lastrowid


def add_dispo(id_hopital: int, date: datetime, is_dispo: bool):
    conn = mysql.connect(host='localhost', user='root', password='pass', database='annuaire')
    cur = conn.cursor(dictionary=True, prepared=True)
    query = """INSERT INTO annuaire.dispo(id_hopital, num_semaine, is_dispo) values (%s, %s, %s);"""
    cur.execute(query, [id_hopital, date, is_dispo])
    conn.commit()
    conn.close()
    return cur.lastrowid


def add_location(longitude: float, latitude: float, id_adresse: int) -> int:
    conn = mysql.connect(host='localhost', user='root', password='pass', database='annuaire')
    cur = conn.cursor(dictionary=True, prepared=True)
    query = """INSERT INTO annuaire.location(longitude, latitude, id_adresse) VALUES (%s, %s, %s);"""
    cur.execute(query, [longitude, latitude, id_adresse])
    conn.commit()
    conn.close()
    return cur.lastrowid


def add_pays(nom_pays: str) -> None | int:
    try:
        conn = mysql.connect(host='localhost', user='root', password='pass', database='annuaire')
        cur = conn.cursor(dictionary=True, prepared=True)
        query = """INSERT INTO annuaire.pays(nom_pays) VALUES (%s);"""
        cur.execute(query, [nom_pays])
        conn.commit()
        conn.close()
        return cur.lastrowid
    except Exception as e:
        print(e)
        return None


def add_region(nom_region: str, id_pays: int) -> int | None:
    try:
        conn = mysql.connect(host='localhost', user='root', password='pass', database='annuaire')
        cur = conn.cursor(dictionary=True, prepared=True)
        query = """INSERT INTO annuaire.region(nom_region, id_pays) VALUES (%s, %s);"""
        cur.execute(query, (nom_region, id_pays))
        conn.commit()
        conn.close()
        return cur.lastrowid
    except Exception as e:
        print(e)
        return None


def get_id_adresse_by_(adresse: str, cdp: str, ville: str) -> int:
    conn = mysql.connect(host='localhost', user='root', password='pass', database='annuaire')
    cur = conn.cursor(dictionary=True, prepared=True)
    query = """SELECT id FROM annuaire.adresse WHERE adresse = %s and cdp = %s and ville = %s;"""
    cur.execute(query, [adresse, cdp, ville])
    adresse_id = cur.fetchone()
    conn.close()
    return adresse_id['id'] if adresse_id else None


def get_id_region_by_name(nom_region: str) -> int | None:
    conn = mysql.connect(host='localhost', user='root', password='pass', database='annuaire')
    cur = conn.cursor(dictionary=True, prepared=True)
    query = """SELECT id FROM annuaire.region WHERE nom_region = %s;"""
    cur.execute(query, [nom_region])
    region_id = cur.fetchone()
    conn.close()
    return region_id['id'] if region_id else None


def get_id_pays_by_name(nom_pays: str) -> int:
    conn = mysql.connect(host='localhost', user='root', password='pass', database='annuaire')
    cur = conn.cursor(dictionary=True, prepared=True)
    query = """SELECT id FROM annuaire.pays WHERE nom_pays = %s;"""
    cur.execute(query, [nom_pays])
    pays_id = cur.fetchone()
    conn.close()
    return pays_id['id'] if pays_id else None


def get_all_hopital_by_region(region: str) -> List:
    conn = mysql.connect(host='localhost', user='root', password='pass', database='annuaire')
    cur = conn.cursor(dictionary=True, prepared=True)
    query = """
       SELECT annuaire.hopital.nom_hopital,
       adresse.adresse,
       adresse.cdp,
       adresse.ville,
       location.longitude,
       location.latitude,
       hopital.tel,
       region.nom_region,
       pays.nom_pays
from annuaire.hopital
         left join annuaire.adresse on adresse.id = annuaire.hopital.id_adresse
         left join annuaire.location on adresse.id = location.id_adresse
         left join annuaire.region on adresse.id_region = region.id
         left join annuaire.pays on region.id_pays = pays.id
        where nom_region =  %s;
    """
    cur.execute(query, (region,))
    hopital = cur.fetchall()
    conn.commit()
    return hopital


def get_all_hopital_by_dept(dept_nom: str) -> list:
    conn = mysql.connect(host='localhost', user='root', password='pass', database='annuaire')
    cur = conn.cursor(dictionary=True, prepared=True)
    query = """ 
     SELECT hopital.nom_hopital,
       adresse.adresse,
       adresse.cdp,
       adresse.ville,
       location.longitude,
       location.latitude,
       hopital.tel,
       region.nom_region,
       pays.nom_pays
from annuaire.hopital
         left join annuaire.adresse on adresse.id = hopital.id_adresse
         left join annuaire.location on adresse.id = location.id_adresse
         left join annuaire.region on adresse.id_region = region.id
         left join annuaire.pays on region.id_pays = pays.id    
        WHERE adresse.cdp LIKE %s;"""
    cur.execute(query, (dept_nom + "%",))
    hopitals = cur.fetchall()
    conn.close()
    return hopitals


def get_list_of_region():
    conn = mysql.connect(host='localhost', user='root', password='pass', database='annuaire')
    cur = conn.cursor(dictionary=True, prepared=True)
    query = """SELECT id, nom_region FROM annuaire.region;"""
    cur.execute(query)
    regions = cur.fetchall()
    conn.close()
    return regions


def get_all_hopital_by_dispo(region: str | None, departement: str | None, is_libre: str | None, week_nb: int | None):
    conn = mysql.connect(host='localhost', user='root', password='pass', database='annuaire')
    cur = conn.cursor(dictionary=True, prepared=True)
    query = """
    SELECT 
    hopital.id,
    hopital.nom_hopital,
       adresse.adresse,
       adresse.cdp,
       adresse.ville,
       location.longitude,
       location.latitude,
       hopital.tel,
       region.nom_region,
       pays.nom_pays,
       date_dispo.date_debut,
       date_dispo.date_fin,
       date_dispo.id_num_semaine,
       dispo.is_dispo
from annuaire.hopital
         left join annuaire.adresse on adresse.id = hopital.id_adresse
         left join annuaire.location on adresse.id = location.id_adresse
         left join annuaire.region on adresse.id_region = region.id
         left join annuaire.pays on region.id_pays = pays.id
         left join annuaire.dispo dispo on hopital.id = dispo.id_hopital
         left join annuaire.date_dispo date_dispo on date_dispo.id = dispo.num_semaine
    """

    params = []

    if region is not None:
        query += "WHERE " if len(params) == 0 else "AND "

        query = query + " region.id = %s "
        params.append(region)

    if departement is not None:
        query += "WHERE " if len(params) == 0 else "AND "
        query += " adresse.cdp LIKE %s "
        params.append(f"{departement}%")

    if is_libre is not None:
        query += "WHERE " if len(params) == 0 else "AND "
        query += " dispo.is_dispo = %s "
        params.append(is_libre)

    if week_nb is not None:
        query += "WHERE " if len(params) == 0 else "AND "
        query += " date_dispo.id_num_semaine = %s "
        params.append(week_nb)

    query += ";"
    if len(params) > 0:
        cur.execute(query, params)
    else:
        cur.execute(query)
    results = cur.fetchall()
    conn.close()
    return results


def get_number_of_hospitals() -> int:
    conn = mysql.connect(host='localhost', user='root', password='pass', database='annuaire')
    cur = conn.cursor(dictionary=True, prepared=True)
    query = """SELECT count(*) as nb from annuaire.hopital;"""
    cur.execute(query)
    nb = cur.fetchone()['nb']
    conn.close()
    return nb


def insert_hospital_dispo(id_hopital: int, departement: int, isLibre: int) -> None:
    conn = mysql.connect(host='localhost', user='root', password='pass', database='annuaire')
    cur = conn.cursor(dictionary=True, prepared=True)
    query = """INSERT INTO annuaire.dispo (id_hopital, num_semaine, is_dispo) values (%s, %s, %s);"""
    cur.execute(query, (id_hopital, departement, isLibre))
    conn.commit()
    conn.close()


def get_hospital_dispo(id_hopital: int, departement: int, isLibre: int) -> tuple:
    conn = mysql.connect(host='localhost', user='root', password='pass', database='annuaire')
    cur = conn.cursor(dictionary=True, prepared=False)
    query = """SELECT id FROM annuaire.dispo
     WHERE id_hopital = %s AND num_semaine = %s AND is_dispo = %s;"""
    cur.execute(query, (id_hopital, departement, isLibre))
    hopitals = cur.fetchone()
    conn.close()
    return hopitals


def get_all_date_week():
    conn = mysql.connect(host='localhost', user='root', password='pass', database='annuaire')
    cur = conn.cursor(dictionary=True, prepared=True)
    query = """SELECT date_dispo.id, date_dispo.id_num_semaine, date_debut, date_fin FROM annuaire.date_dispo;"""
    cur.execute(query)
    dates = cur.fetchall()
    conn.close()
    return dates


def get_week_number(date_debut: datetime, date_fin: datetime | None = None) -> List:
    params = [date_debut]
    conn = mysql.connect(host='localhost', user='root', password='pass', database='annuaire')
    cur = conn.cursor(dictionary=True, prepared=True)
    query = """select date_dispo.id_num_semaine from annuaire.date_dispo """
    if date_fin is None:
        query += " where date_debut >= %s;"
    if date_fin is not None:
        query += " where date_debut >= %s and date_fin <= %s;"
        params.append(date_fin)
    cur.execute(query, params)
    dates = cur.fetchall()
    conn.close()
    return dates


def get_hopital_by_id(id_hopital: int) -> list:
    conn = mysql.connect(host='localhost', user='root', password='pass', database='annuaire')
    cur = conn.cursor(dictionary=True, prepared=True)

    query = """
        SELECT hopital.id, hopital.nom_hopital,
           adresse.adresse,
           adresse.cdp,
           adresse.ville,
           location.longitude,
           location.latitude,
           hopital.tel,
           region.nom_region,
           pays.nom_pays

    from annuaire.hopital
             left join annuaire.adresse on adresse.id = hopital.id_adresse
             left join annuaire.location on adresse.id = location.id_adresse
             left join annuaire.region on adresse.id_region = region.id
             left join annuaire.pays on region.id_pays = pays.id
             left join annuaire.dispo dispo on hopital.id = dispo.id_hopital
             left join annuaire.date_dispo date_dispo on date_dispo.id = dispo.num_semaine
             where hopital.id = %s;
        """
    cur.execute(query, (id_hopital,))
    hopital = cur.fetchone()
    conn.close()
    return hopital


def get_disponibilite_by_id(id_hopital: int, date_debut: datetime | None = None, date_fin: datetime | None = None,
                            limit: int | None = None) -> list:
    params = [id_hopital]
    conn = mysql.connect(host='localhost', user='root', password='pass', database='annuaire')
    cur = conn.cursor(dictionary=True)
    query = """
               SELECT 
               date_dispo.id_num_semaine as num_semaine,
               date_dispo.date_debut,
               date_dispo.date_fin,
               dispo.is_dispo
           from annuaire.dispo
           left join annuaire.date_dispo date_dispo on date_dispo.id = dispo.num_semaine
        where dispo.id_hopital = %s
               """

    if date_fin is None:
        query += " and date_debut >= %s"
        params.append(date_debut)
    if date_fin is not None and date_fin is None:
        query += " and date_debut >= %s and date_fin <= %s"
        params.append(date_fin)
    if limit is not None:
        query += " limit %s"
        params.append(limit)
    query += ';'

    cur.execute(query, params)
    disponibilites = cur.fetchall()
    conn.close()
    return disponibilites
