DROP DATABASE IF EXISTS annuaire;
CREATE DATABASE annuaire;
USE annuaire;

CREATE TABLE pays
(
    id       INT PRIMARY KEY AUTO_INCREMENT,
    nom_pays VARCHAR(65) UNIQUE
);

CREATE TABLE region
(
    id         INT PRIMARY KEY AUTO_INCREMENT,
    nom_region VARCHAR(65) UNIQUE,
    id_pays    INT,
    FOREIGN KEY (id_pays) REFERENCES pays (id)
);


CREATE TABLE adresse
(
    id      INT PRIMARY KEY AUTO_INCREMENT,
    adresse VARCHAR(65),
    cdp     VARCHAR(5),
    ville   VARCHAR(65),
    id_region INT,
    FOREIGN KEY (id_region) REFERENCES region (id)
);

CREATE TABLE hopital
(
    id          INT PRIMARY KEY AUTO_INCREMENT,
    nom_hopital VARCHAR(255),
    tel         VARCHAR(65),
    id_adresse  INT,
    FOREIGN KEY (id_adresse) REFERENCES adresse (id)
);

CREATE TABLE date_dispo
(
    id int PRIMARY KEY AUTO_INCREMENT,
    date_debut DATE,
    date_fin DATE,
    id_num_semaine int
);

CREATE TABLE dispo
(
    id         INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
    id_hopital INT             NOT NULL,
    num_semaine INT NOT NULL ,
    is_dispo   BOOL NOT NULL ,
    FOREIGN KEY (id_hopital) REFERENCES hopital (id),
    FOREIGN KEY (num_semaine) REFERENCES date_dispo (id)
);


CREATE TABLE location
(
    id         INT PRIMARY KEY AUTO_INCREMENT,
    longitude  FLOAT,
    latitude   FLOAT,
    id_adresse INT,
    FOREIGN KEY (id_adresse) REFERENCES adresse (id)
);


