from datetime import date

import dash_bootstrap_components as dbc
import pandas as pd
import plotly.express as px
from dash import Dash, html, dcc, Output, Input

app = Dash(__name__, external_stylesheets=[dbc.themes.BOOTSTRAP, dbc.icons.FONT_AWESOME])

API_URL = 'http://127.0.0.1:8000'
date_jour = date.today()

regions = [{'value': '', 'label': ''}]

disponibilite = [
    {'value': '', 'label': ''},
    {'value': '0', 'label': 'Indisponible'},
    {'value': "1", 'label': 'Disponible'}
]


def load_region():
    global regions
    try:
        df_regions = pd.read_json(f"{API_URL}/region", orient='records')
        df_regions.rename(columns={'id': 'value', 'nom_region': 'label'}, inplace=True)
        regions += df_regions.to_dict(orient='records')
    except Exception as e:
        regions = [{'value': '', 'label': ''}]
        print(e)


load_region()

app.layout = dbc.Container(html.Div(children=[
    html.H1(children='Carte de disponibilité des hopitaux de France', style={'textAlign': 'center'}),
    html.Div(style={'display': 'flex'}, children=[
        html.Div(style={'width': '30%'}, children=[

            html.Div(children=[
                dbc.Form([
                    html.Div(children=[
                        dbc.Label('Région', html_for='dropdown-region-selection'),
                        dbc.Select(options=regions, value='', id='dropdown-region-selection',
                                   placeholder='Selectionnez une région'),

                    ]),

                    html.Div(children=[
                        dbc.Label('Numéro de département (2 à 5 chiffres) ', html_for='input-departement'),

                        dbc.Input(id="input-departement", type="text", placeholder="Entrez un numéro de département",
                                  debounce=True,
                                  minLength=2,
                                  maxLength=5,
                                  pattern=u"^[0-9]{0-5}$",

                                  ),
                    ]),

                    html.Div(children=[
                        dbc.Label('Disponibilité', html_for='dropdown-disponible-selection'),
                        dbc.Select(options=disponibilite, value='', id='dropdown-disponible-selection',
                                   placeholder='Selectionnez une disponibilité'),
                    ]),

                    html.Div([
                        dbc.Label(html_for='my-date-picker-range', style={'textAlign': 'center'},
                                  children='Période'),

                        dcc.DatePickerRange(
                            id='my-date-picker-range',
                            display_format='DD/MM/YYYY',
                            clearable=True,
                            with_portal=True,
                            start_date=date_jour,
                            first_day_of_week=1,
                            minimum_nights=5,
                            start_date_placeholder_text='date de début',
                            end_date_placeholder_text='Date de fin'

                        ),
                    ]),
                ]),
            ]),
        ]),

        html.Div(style={'width': '70%'}, children=[
            dbc.Spinner(html.Div(id="loading-output")),
            dbc.Label(children="Cacher la carte"),
            dbc.Switch(id="input-switch", value=True, className="d-inline-block ms-1", persistence=True),

            html.Div(id='output-test'),
        ]),
    ]),
])
)


def fetch_hospitals(region, status, start_date, end_date, departement):
    query = ""
    param = 0
    hopitals = []

    if departement is not None and departement != '':
        query += "?" if param == 0 else "&"
        query += "departement=" + str(departement)
        param += 1
    if region is not None and region != '':
        query += "?" if param == 0 else "&"
        query += "region=" + str(region)
        param += 1
    if status is not None and status != '':
        query += "?" if param == 0 else "&"
        query += "is_libre=" + str(status)
        param += 1

    if start_date is not None and start_date != '':
        query += "?" if param == 0 else "&"
        query += "date_debut=" + str(start_date)
        param += 1

    if end_date is not None and end_date != '':
        query += "?" if param == 0 else "&"
        query += "date_fin=" + str(end_date)
        param += 1

    if region != '' or status != '' or end_date is not None or departement is not None:
        pass
    try:
        df_hospitals = pd.read_json(f"{API_URL}/hopital/dispo/{query}")

        df_hospitals.rename(columns={'id': 'value', 'nom_region': 'label'}, inplace=True)
        hopitals = df_hospitals.to_dict(orient='records')
    except Exception as e:
        print(e)
    return hopitals
    pass


@app.callback(Output(component_id='output-test', component_property='children'),
              Input(component_id='dropdown-region-selection', component_property='value'),
              Input(component_id='dropdown-disponible-selection', component_property='value'),
              Input(component_id='my-date-picker-range', component_property='start_date'),
              Input(component_id='my-date-picker-range', component_property='end_date'),
              Input(component_id="input-departement", component_property='value'),
              Input(component_id="input-switch", component_property='value'),
              )
def get_disponibilite_selection(region, status, start_date, end_date, departement, switchStatus):
    hospitals = fetch_hospitals(region, status, start_date, end_date, departement)
    if len(hospitals) != 0:
        table = show_hospitals_list(hospitals)

        df_hospitals = pd.DataFrame(hospitals)
        df_hospitals.rename({'cdp': 'Code postal'}, axis='columns', inplace=True)
        band_colors_list = ['green', 'gray', 'blue']
        fig = px.scatter_mapbox(df_hospitals, lat="latitude", lon="longitude",
                                hover_name="nom_hopital",
                                hover_data=["adresse", "Code postal", "ville", "tel"],
                                color_discrete_sequence=band_colors_list,
                                zoom=6,
                                height=600, width=800,
                                )
        fig.update_layout(mapbox_style="open-street-map")
        fig.update_layout(margin={"r": 0, "t": 0, "l": 200, "b": 0})
        fig.update_layout()

        return html.Div(children=[
            dcc.Graph(figure=fig) if switchStatus == True else html.Div(children=[]),

            table,
        ])
    else:
        return html.Div(children=[])


def show_hospitals_list(hospitals):
    table_header = [
        html.Thead(html.Tr(
            [html.Th("Hopital"), html.Th("Adresse"), html.Th("Code postal"), html.Th("Ville"), html.Th("Téléphone"),
             html.Th("Disponibilité")]))
    ]
    rows = []
    for hospital in hospitals:
        disponibilite = hospital['disponibilites']
        disponibilite = f" {disponibilite[0]['is_disponibilite']} semaine: {disponibilite[0]['num_semaine']}"

        row = html.Tr([html.Td(hospital['nom_hopital']), html.Td(hospital['adresse']), html.Td(hospital['cdp']),
                       html.Td(hospital['ville']), html.Td(hospital['tel']), html.Td(disponibilite)])
        rows.append(row)
    table_body = [html.Tbody(rows)]
    table = dbc.Table(table_header + table_body, bordered=True)
    return table


if __name__ == '__main__':
    load_region()

    app.run(debug=True)
