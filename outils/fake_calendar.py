import random

from commun.database.db_queries import get_number_of_hospitals, insert_hospital_dispo

nb_hospital = get_number_of_hospitals()
if __name__ == "__main__":
    for week_number in range(1, 53):
        for id_hopital in range(1, nb_hospital):
            isDispo = random.randint(0, 1)

            print(
                f'id_hopital: {id_hopital}, nums_semaine: {week_number}, isDispo: {isDispo}, nb_hospital: {nb_hospital}')
            insert_hospital_dispo(id_hopital, week_number, isDispo)
