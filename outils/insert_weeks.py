from isoweek import Week

from commun.database.db_queries import add_date_dispo


def insert_date_dispo(date_debut, date_fin, num_semaine):
    try:
        add_date_dispo(date_debut, date_fin, num_semaine)
    except Exception as e:
        print(e)
    pass


if __name__ == '__main__':
    week_list = range(1, 54)
    for week in week_list:
        w = Week(2024, week)
        print("La semaine  %s commence le %s" % (w, w.monday()))
        print("La semaine %s se termine le %s" % (w, w.sunday()))

        insert_date_dispo(w.monday(), w.sunday(), week)
