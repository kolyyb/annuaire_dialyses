import time

import mysql.connector as mysql
import requests

API_URL = "https://api-adresse.data.gouv.fr/search/?q="


def get_all_address():
    conn = mysql.connect(host='localhost', user='root', password='pass', database='annuaire')
    cur = conn.cursor(dictionary=True, prepared=True)
    query = """SELECT id, adresse, cdp, ville, id_region FROM annuaire.adresse ;"""
    cur.execute(query)
    adresses = cur.fetchall()
    conn.close()
    return adresses


def replace_space(adresse):
    return adresse.replace(' ', '+')


def get_gps_coordonate(adresse, code_postal):
    response = requests.get(f'{API_URL}{replace_space(adresse)}&postcode={code_postal}')

    print(replace_space(adresse), code_postal)
    if response.status_code == 200:
        data = response.json()
        if (data is not None and data['features'] is not None and len(data['features']) > 0 and
                data['features'][0]['geometry'] is not None and data['features'][0]['geometry'][
                    'coordinates'] is not None):
            return data['features'][0]['geometry']['coordinates'][0], data['features'][0]['geometry']['coordinates'][1]

        pass

    return None, None


def insert_coodonate_gps(latitude, longitude, id_adresse):
    conn = mysql.connect(host='localhost', user='root', password='pass', database='annuaire')
    cur = conn.cursor(dictionary=True, prepared=True)
    query = """INSERT INTO annuaire.location2(id,longitude, latitude, id_adresse) VALUES(%s,%s, %s, %s);"""
    cur.execute(query, (id_adresse, longitude, latitude, id_adresse))
    conn.commit()
    conn.close()
    return cur.lastrowid


if __name__ == "__main__":
    adresses = get_all_address()
    for adress in adresses:
        lon, lat = get_gps_coordonate(adress['adresse'], adress['cdp'])
        print(lat, lon)
        last_id = insert_coodonate_gps(lat, lon, adress['id'])
        print(last_id)
        time.sleep(5)
